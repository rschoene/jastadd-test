// Test that the flushing API is working together with incremental evaluation
// .options: tracing=flush cache=all rewrite=cnta incremental=param
// .source: 1.8
import static runtime.Test.*;

public class Test {
  public static void main(String[] args) {
    test(new A());
    test(new B());
  }

  private static void test(final A subjectUnderTest) {
    subjectUnderTest.setToken(1);
    final int[] counter = {0};
    subjectUnderTest.trace().setReceiver(new ASTState.Trace.Receiver() {
      @Override
      public void accept(ASTState.Trace.Event event, ASTNode node, String attribute, Object params, Object value) {
        testSame(ASTState.Trace.Event.INC_FLUSH_ATTR, event);
        testSame(node, subjectUnderTest);
        testEquals("value", attribute);
        counter[0]++;
      }
    });
    // counter is still zero, as there was no flush yet
    int actualValue = subjectUnderTest.value();
    testEquals(1, actualValue);
    testEquals(0, counter[0]);

    // no change to token, so value is read from cache, and no flush
    actualValue = subjectUnderTest.value();
    testEquals(1, actualValue);
    testEquals(0, counter[0]);

    // after change to token, value needs to be recomputed
    subjectUnderTest.setToken(5);
    actualValue = subjectUnderTest.value();
    testEquals(5, actualValue);
    testEquals(1, counter[0]);
  }
}
